string PeriodToString(ENUM_TIMEFRAMES pPeriod) {
    switch(pPeriod) {
        case PERIOD_M1:
            return "1 minute";
        case PERIOD_M2:
            return "2 minutes";
        case PERIOD_M3:
            return "3 minutes";
        case PERIOD_M4:
            return "4 minutes";
        case PERIOD_M5:
            return "5 minutes";
        case PERIOD_M6:
            return "6 minutes";
        case PERIOD_M10:
            return "10 minutes";
        case PERIOD_M12:
            return "12 minutes";
        case PERIOD_M15:
            return "15 minutes";
        case PERIOD_M20:
            return "20 minutes";
        case PERIOD_M30:
            return "30 minutes";
        case PERIOD_H1:
            return "1 hour";
        case PERIOD_H2:
            return "2 hours";
        case PERIOD_H3:
            return "3 hours";
        case PERIOD_H4:
            return "4 hours";
        case PERIOD_H6:
            return "6 hours";
        case PERIOD_H8:
            return "8 hours";
        case PERIOD_H12:
            return "12 hours";
        case PERIOD_D1:
            return "1 day";
        case PERIOD_W1:
            return "1 week";
        case PERIOD_MN1:
            return "1 month";
    }
    return "";
}