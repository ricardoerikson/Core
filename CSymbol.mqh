#include <Trade\SymbolInfo.mqh>

class CSymbol
{
public:
    CSymbol(const string pSymbol);
    CSymbol(const CSymbol &symbol);
    string name() const;
    double margin() const;
    double tickSize() const;
    double contractSize() const;
    double lotsMin() const;
    double lotsMax() const;
    double lotsStep() const;
    double stopLevel() const;
    int digits() const;
    string tradeModeDescription() const;
    ENUM_SYMBOL_TRADE_MODE tradeMode() const;
    void print() const;
private:
    CSymbolInfo m_symbolInfo;

};

CSymbol::CSymbol(const string pSymbol) {
    m_symbolInfo.Name(pSymbol);
    m_symbolInfo.RefreshRates();
}

CSymbol::CSymbol(const CSymbol &pSymbol) {
    m_symbolInfo.Name(pSymbol.name());
    m_symbolInfo.RefreshRates();
}

string CSymbol::name() const {
    return m_symbolInfo.Name();
}

double CSymbol::margin() const {
    return m_symbolInfo.MarginInitial();
}

double CSymbol::tickSize() const {
    return m_symbolInfo.TickSize();
}

double CSymbol::contractSize() const {
    return m_symbolInfo.ContractSize();
}
double CSymbol::lotsMin() const {
    return m_symbolInfo.LotsMin();
}
double CSymbol::lotsMax() const {
    return m_symbolInfo.LotsMax();
}
double CSymbol::lotsStep() const {
    return m_symbolInfo.LotsStep();
}

ENUM_SYMBOL_TRADE_MODE CSymbol::tradeMode() const {
    return m_symbolInfo.TradeMode();
}

string CSymbol::tradeModeDescription() const {
    return m_symbolInfo.TradeModeDescription();
}

int CSymbol::digits() const {
    return (int) m_symbolInfo.Digits();
}

double CSymbol::stopLevel() const {
    return SymbolInfoInteger(name(), SYMBOL_TRADE_STOPS_LEVEL) * tickSize();
}

void CSymbol::print() const {
    string strTemplate = "Symbol: %s\nTick size: %.2f\nDigits: %.2f\nMargin: %.2f\nLots min:%.0f\nLots max: %.0f\nLots step: %.0f\nTrade mode: %s";
    string name      = name();
    double margin    = margin();
    double tickSize  = tickSize();
    double digits    = digits();
    double lotsMin   = lotsMin();
    double lotsMax   = lotsMax();
    double lotsStep  = lotsStep();
    string tradeMode = tradeModeDescription();
    PrintFormat(strTemplate, name, tickSize, digits, margin, lotsMin, lotsMax, lotsStep, tradeMode);
}