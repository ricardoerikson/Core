#include <Core/Utils/ArrayUtils.mqh>
#include <Core/CSymbol.mqh>

#include <MarketProfile/Core/CTimeframe.mqh>

class CTimeSeries {

public:
    CTimeSeries(const CSymbol &pSymbol, ENUM_TIMEFRAMES pTimeframe, int pStartPos, int pCount);
    CTimeSeries(const CSymbol &pSymbol, ENUM_TIMEFRAMES pTimeframe, datetime pFrom, int pCount);
    CTimeSeries(const CSymbol &pSymbol, ENUM_TIMEFRAMES pTimeframe, datetime pFrom, datetime pTo);
    CTimeframe timeframe(int pPos = 0) const;
    double high(int pPos = 0) const;
    double low(int pPos = 0) const;
    double open(int pPos = 0) const;
    double close(int pPos = 0) const;
    int count() const;
    void refresh();

private:
    const CSymbol m_symbol;
    const ENUM_TIMEFRAMES m_timeframe;
    int m_startPos;
    int m_count;
    datetime m_from, m_to;
    double m_high[];
    double m_low[];
    double m_open[];
    double m_close[];
};

CTimeSeries::CTimeSeries(const CSymbol &pSymbol, ENUM_TIMEFRAMES pTimeframe, int pStartPos, int pCount) :
    m_symbol(pSymbol),
    m_timeframe(pTimeframe),
    m_startPos(pStartPos),
    m_count(pCount) {
    refresh();
}

CTimeSeries::CTimeSeries(const CSymbol &pSymbol, ENUM_TIMEFRAMES pTimeframe, datetime pFrom, int pCount) :
    m_symbol(pSymbol),
    m_timeframe(pTimeframe),
    m_from(pFrom) {
    int n = Bars(m_symbol.name(), pTimeframe, pFrom, TimeCurrent());
    m_count = MathMin(n, pCount);
    m_startPos = n - m_count;
    refresh();
}

CTimeSeries::CTimeSeries(const CSymbol &pSymbol, ENUM_TIMEFRAMES pTimeframe, datetime pFrom, datetime pTo):
    m_symbol(pSymbol),
    m_timeframe(pTimeframe),
    m_from(pFrom),
    m_to(pTo) {
    m_count = Bars(m_symbol.name(), pTimeframe, pFrom, pTo);
    m_startPos = Bars(m_symbol.name(), pTimeframe, pTo, TimeCurrent());

    refresh();
}

CTimeframe CTimeSeries::timeframe(int pPos) const {
    double open = open(pPos);
    double high = high(pPos);
    double low = low(pPos);
    double close = close(pPos);
    CTimeframe timeframe(open, high, low, close);
    return timeframe;
}

double CTimeSeries::high(int pPos) const {
    return m_high[MathMin(pPos, count())];
}

double CTimeSeries::low(int pPos) const {
    return m_low[MathMin(pPos, count())];
}

double CTimeSeries::open(int pPos) const {
    return m_open[MathMin(pPos, count())];
}

double CTimeSeries::close(int pPos) const {
    return m_close[MathMin(pPos, count())];
}

int CTimeSeries::count() const {
    return ArraySize(m_high);
}

void CTimeSeries::refresh() {
    CopyHigh(m_symbol.name(), m_timeframe, m_startPos, m_count, m_high);
    CopyLow(m_symbol.name(), m_timeframe, m_startPos, m_count, m_low);
    CopyOpen(m_symbol.name(), m_timeframe, m_startPos, m_count, m_open);
    CopyClose(m_symbol.name(), m_timeframe, m_startPos, m_count, m_close);
}